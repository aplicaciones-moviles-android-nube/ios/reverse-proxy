# Utiliza una imagen de Nginx como base
FROM nginx:latest

# Elimina la configuración predeterminada de Nginx
RUN rm -rf /etc/nginx/conf.d/*

# Copia el archivo de configuración personalizado de Nginx que incluye la configuración del proxy inverso
COPY nginx.conf /etc/nginx/conf.d/default.conf
